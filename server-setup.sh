#!/bin/bash 

# 
# Shell script to auto-provision a fresh server for software engineering 1 2020
# installs: nginx, certbot, python, mongodb, nginx & normal updates on ubuntu 18.04
# takes positional argument $1 for subdoamin of server, supported: dev, staging
# 
# v0.1 - 2020.02.14
# creates: team23 user, nginx config file based on positional argument, certbot certificates
# 

# $1 = 1-4, team11,21 ...
# $2 = supply/demand

# set root password
echo "Set Root Password"
passwd

# install apps and deps
wget -qO - https://www.mongodb.org/static/pgp/server-4.2.asc | apt-key add -
apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 9DA31620334BD75D9DCB49F368818C72E52529D4
echo "deb [ arch=amd64 ] https://repo.mongodb.org/apt/ubuntu bionic/mongodb-org/4.2 multiverse" | tee /etc/apt/sources.list.d/mongodb-org-4.2.list
add-apt-repository universe
apt-add-repository ppa:certbot/certbot --yes
apt update && apt upgrade --yes
apt install nginx-full certbot python-certbot-nginx mongodb-org --yes
apt install mysql-client docker docker-compose --yes
apt autoremove --yes

echo "mongodb-org hold" | dpkg --set-selections
echo "mongodb-org-server hold" | dpkg --set-selections
echo "mongodb-org-shell hold" | dpkg --set-selections
echo "mongodb-org-mongos hold" | dpkg --set-selections
echo "mongodb-org-tools hold" | dpkg --set-selections

# create team1x user
echo "User: team1$1"
useradd team1$1 -s /bin/bash
groupadd team1$1
mkdir -p /home/team1$1
chown team1$1:team1$1 /home/team1$1
passwd team1$1
usermod -aG sudo team1$1

# create team2x user
echo "User: team2$1"
useradd team2$1 -s /bin/bash
groupadd team2$1
mkdir -p /home/team2$1
chown team2$1:team2$1 /home/team2$1
passwd team2$1
usermod -aG sudo team2$1

# setup groups for www-data to allow serving from no ownership (lazy)
usermod -aG team1$1 www-data
usermod -aG team2$1 www-data

# setup nginx config
ln $2/$2.team1$1.softwareengineeringii.com /etc/nginx/sites-available/
ln $2/$2.team2$1.softwareengineeringii.com /etc/nginx/sites-available/
ln -s /etc/nginx/sites-available/$2.team1$1.softwareengineeringii.com /etc/nginx/sites-enabled
ln -s /etc/nginx/sites-available/$2.team2$1.softwareengineeringii.com /etc/nginx/sites-enabled

# certbot for new domain
certbot --nginx -d $2.team1$1.softwareengineeringii.com -n --agree-tos -m webmaster@softwareengineeringii.com
certbot --nginx -d $2.team2$1.softwareengineeringii.com -n --agree-tos -m webmaster@softwareengineeringii.com

systemctl restart nginx
